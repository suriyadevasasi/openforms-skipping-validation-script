// Checking if condition is fulfilled.
if (checkDocuments(data))
{
   // Getting batch field named 'cc_SkipValidation'.
   DOKuStar.Data.Xml.Bool skipValidationField = data.RootNode.Fields["cc_SkipValidation"] as DOKuStar.Data.Xml.Bool;
   // Setting new value.
   skipValidationField.SetValue(true);
   skipValidationField.State = DataState.Ok;
}
// !!! Closing bracket is neccessary !!!
}
// ------------------ Functions
private bool checkDocuments(DataPool dataPool)
{
   foreach (Document doc in dataPool.RootNode.Documents)
   {
// Getting the field named 'Rodzaj Paliwa'
      Field rpFld = (Field)doc.Fields["Rodzaj_paliwa"];

      // Getting the field named 'Numer Faktury'
      Field nfFld = (Field)doc.Fields["Nr_faktury"];

      // Getting the field named 'Data Wystawienia'
      Field dwFld = (Field)doc.Fields["Data_wystawienia"];

       // Getting the field named 'Rodzaj Pojazdu'
      Field rpoFld = (Field)doc.Fields["Rodzaj_pojazdu"];
       
      // If the field does not exists or has the state error,
      // return false


	if (rpFld == null || rpFld.State != DataState.Ok)
      {
          return false;
      }

	if (nfFld == null || nfFld.State != DataState.Ok)
      {
          return false;
      }

	if (dwFld == null || dwFld.State != DataState.Ok)
      {
          return false;
      }


	if (rpoFld == null || rpoFld.State != DataState.Ok)
      {
          return false;
      }


   }
   return true;
 
// !!! No closing bracket needed !!!

